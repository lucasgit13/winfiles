"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plugins 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
call plug#begin()

Plug 'vim-airline/vim-airline'
" Plug 'itchyny/lightline.vim'
Plug 'tpope/vim-fugitive'
Plug 'ap/vim-css-color'
Plug 'vifm/vifm.vim'
Plug 'ryanoasis/vim-devicons'
Plug 'scrooloose/nerdtree'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'gorodinskiy/vim-coloresque'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'vim-python/python-syntax'
" Plug 'pappasam/coc-jedi', { 'do': 'yarn install --frozen-lockfile && yarn build', 'branch': 'main' }
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'nvim-lua/telescope.nvim'
Plug 'nvim-lua/plenary.nvim'
" Plug 'scrooloose/syntastic'
" Plug 'racer-rust/vim-racer'
Plug 'rust-lang/rust.vim'
" Plug 'dense-analysis/ale'
" Plug 'Valloric/YouCompleteMe'
" Plug 'scrooloose/syntastic'
" Plug 'cespare/vim-toml'
" Plug 'vimwiki/vimwiki'
Plug 'morhetz/gruvbox'
Plug 'tpope/vim-commentary'
Plug 'jiangmiao/auto-pairs'
Plug 'kien/rainbow_parentheses.vim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'jiangmiao/auto-pairs'

call plug#end()
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Leader key
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let mapleader=" "
nnoremap <SPACE> <Nop>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" NERDtree remap keys
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nnoremap <C-n> :NERDTreeToggle<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:python_highlight_all = 1

" Always show statusline
set laststatus=2
" Remove the standard statusline
set noshowmode

""""""""""""""""""""""""""""""""""""""
" Rust
""""""""""""""""""""""""""""""""""""""
set completeopt=menu,menuone,preview,noselect,noinsert
" let g:ale_completion_enabled = 1
" let g:ale_linters = {'rust': ['analyzer']}
" let g:ale_fixers = { 'rust': ['rustfmt', 'trim_whitespace', 'remove_trailing_lines'] }
" nnoremap <Leader>gd :ALEGoToDefinition<CR>

""""""""""""""""""""""""""""""""""""""
" The lightline.vim theme
""""""""""""""""""""""""""""""""""""""
" let g:lightline = {
"       \ 'colorscheme': 'seoul256',
"       \ 'active': {
"       \   'left': [ [ 'mode', 'paste' ],
"       \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
"       \ },
"       \ 'component_function': {
"       \   'gitbranch': 'FugitiveHead'
"       \ },
"       \ }

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Colorscheme
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:gruvbox_contrast_dark = 'medium'
colorscheme gruvbox 

""""""""""""""""""""""""""""""""""""""
set pumblend=20
hi Pmenu guibg=0
set nocompatible
filetype on
set number relativenumber
set wildmenu		        	" Display all matches when tab complete.
set incsearch                   " Incremental search
set nobackup                    " No auto backups
set noswapfile                  " No swap
set expandtab                   " Use spaces instead of tabs.
set smarttab                    " Be smart using tabs ;)
set smartindent
set autoindent
set tabstop=4
set shiftwidth=4                " One tab == four spaces.
set tabstop=4                   " One tab == four spaces.
set softtabstop=4
set path+=**                    " Searches current directory recursively.
set t_Co=256                    " Set if term supports 256 colors.
set termguicolors
set clipboard+=unnamedplus
set mousemodel=extend
set scrolloff=10
set mouse=a
set hidden
syntax on

autocmd! bufwritepost init.vim source % | echom "Reloaded init.vim"

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Remap Keys
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Remap F3 to set "noh"
nnoremap <ESC> :noh<CR>
" Map F5 to save the current file
map <F5> :w<CR>
" Map F10 to save and quit from the file
map <F10> :wq<CR>

"""""""""""""""""""""""""""""""""""""
"Splits 
"""""""""""""""""""""""""""""""""""""
set splitbelow splitright

" Remap splits navigation with Control + hjkl
nnoremap <Leader>h <C-w>h
nnoremap <Leader>j <C-w>j
nnoremap <Leader>k <C-w>k
nnoremap <Leader>l <C-w>l

" Set Space + wc to close focus split
map <Leader>wc <C-w>c
nnoremap <Tab> >>
nnoremap <S-Tab> <<
vnoremap <Tab> >
vnoremap <S-Tab> <

" Make adjusing split sizes a bit more friendly
noremap <silent> <C-Left> :vertical resize +3<CR>
noremap <silent> <C-Right> :vertical resize -3<CR>
noremap <silent> <C-Up> :resize +3<CR>
noremap <silent> <C-Down> :resize -3<CR>

" Open a terminal in vertical split
map <Leader>tt :vnew term://powershell<CR>i
" Open a terminal in horizontal split
map <Leader>ty :split term://cmd<CR>i

" Change 2 split windows from vert to horiz or horiz to vert
map <Leader>th <C-w>t<C-w>H
map <Leader>tk <C-w>t<C-w>K

"""""""""""""""""""""""""""""""""""""
" Delete with Control-h on insert mode
inoremap <C-h> <Left>
" Delete selected text and paste content on "" register
vnoremap <Leader>d "_dP
vnoremap <Leader>d "_d

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Explore
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
noremap <Leader>ex :Ex<CR>
noremap <Leader>vs :Vexplore<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Buffers
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nnoremap <Leader>en :e $MYVIMRC<CR>
nnoremap <Leader>ev :e C:\Users\lucas\.vimrc<CR>
nnoremap <Leader>bk :bw<CR>
nnoremap <Leader>bl :buffers<CR> :buffer<Space>
nnoremap <Leader>cd :lcd %:p:h<CR>
nnoremap <C-6> <C-^>

" Bind 'ESC' to go to normal mode on terminal
tnoremap <Esc> <C-\><C-n>
" Bind Control-h to go to normal mode and switch to left window
" tnoremap <Leader>h <C-\><C-N><C-w>h
" Bind Control-h to go to normal mode and switch to up window
" tnoremap <Leader>k <C-\><C-N><C-w>k
map <C-j> <C-w>j
tnoremap <C-k> <C-w>k

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vifm
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map <Leader>vv :Vifm<CR>
map <Leader>vs :VsplitVifm<CR>
map <Leader>sp :SplitVifm<CR>
map <Leader>dv :DiffVifm<CR>
map <Leader>tv :TabVifm<CR>

map <Leader>rd :RacerDoc<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vim-Fzf
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" command! -bang PFiles call fzf#vim#files('~/Projects/', <bang>0)
" noremap <Leader>fp :PFiles<CR>

" command! -bang DmenuScripts call fzf#vim#files('~/.dmscripts', <bang>0)
" noremap <Leader>fd :DmenuScripts<CR>

" command! -bang BinScripts call fzf#vim#files('~/.bin', <bang>0)
" noremap <Leader>fb :BinScripts<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" NERDtree remap keys
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" nnoremap <leader>nn :NERDTreeFocus<CR>
" nnoremap <C-n> :NERDTree<CR>
nnoremap <Leader>nt :NERDTreeToggle<CR>
nnoremap <Leader>ff :NERDTreeFind<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Macros
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
autocmd FileType vimwiki inoremap ;b ****<Esc>hi
autocmd FileType python nnoremap <Leader>cc :!python %<CR>
autocmd FileType html inoremap ;p <p><p><Esc>F<i
autocmd FileType html imap ;p <p></p><Esc>F<i
autocmd FileType scss setl iskeyword+=@-@


nmap <silent> gd <Plug>(coc-definition)

