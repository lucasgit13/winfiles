#!/bin/sh

export EDITOR="gvim.bat"
export VISUAL="gvim.bat"

set -o vi 										# Enable vi mode in bash
bind -m vi-insert "\C-l":clear-screen			# Enacle ctrl+l when vimode is set
bind -m vi-insert 'Control-l: clear-screen'		# Control-l: clear-screen

shopt -s autocd 	# Allow you to cd into a directory without typing the whole path
shopt -s cdspell	# autocorrects cd misspellings

if [ -d "$HOME/.bin" ] ;
  then PATH="$HOME/.bin:$PATH"
fi

alias nv='nvim-qt'
# cd /cygdrive/c/Users/lucas

alias k50s='LMX540SK5T8LNBKRAU'
alias j3='4200aa9dc4642300'
alias note='run notepad++'
alias bb='adb shell ./data/local/tmp/busybox'
alias v='gvim.bat'
alias fzf='/usr/bin/fzf'
alias expl='run.exe explorer'
alias h='cd /cygdrive/c/Users/lucas'
alias d='cd /cygdrive/c/Users/lucas/Desktop'
alias sp='sudo pacman'
alias pacs='pacman -Ss'
alias yays='yay -Ss'
alias ll='ls -lah --color=auto --group-directories-first'
alias l.='ls -aC --color=auto --group-directories-first | egrep "^\."'
alias ..='cd ..'
alias ...='cd ../../'
alias .3='cd ../../../'
alias .4='cd ../../../../'

# Config files 
alias bashconf='$EDITOR ~/.bashrc'
alias sb='source ~/.bashrc'
alias vconf='$EDITOR ~/.config/nvim/init.vim'
alias i3conf='$EDITOR ~/.i3/config'
alias xconf='$EDITOR ~/.Xresources'
alias szsh='source ~/.zshrc'
alias zshconfig='$EDITOR ~/.zshrc'
alias qconfig='$EDITOR ~/.config/qutebrowser/config.py'
alias qtileconfig='$EDITOR ~/.config/qtile/config.py'
alias config='git.exe --git-dir=$HOME/winfiles/ --work-tree=$HOME'

# Emacs
alias vw='$EDITOR ~/vimwiki/index.wiki'
alias semacs='.emacs.d/bin/doom sync'
alias e='$EDITOR'

#System information
alias cpu="sensors | awk '/^CPU Temperature:/ {print $3}' &&  ps axch -o cmd:15,%cpu --sort=-%cpu | head"

alias mem='ps axch -o cmd:15,%mem --sort=-%mem | head'

# Move to importants directories
alias master='clear && cd ~/dotfiles/ && ll'
alias patchy='patch -p1 <'

# St 
alias stbackup='rm -rf ~/.programs/st/ && cp -r .config/st/ ~/.programs/' # Create a local backup os config before the patch
alias stdir='cd ~/.config/st/st-0.8.4/ && ll'

# Dmenu
alias dmenuconfig='cd ~/.config/dmenu/dmenu-5.0/ && ll'

#Mount my android devide
alias phone='aft-mtp-mount'
alias lg='aft-mtp-mount ~/Phone/ && cd ~/Phone/Divisão\ interna\ de\ armazenamento/ && ll'
alias lge='cd ~/ && fusermount -u ~/Phone/'

# Build 
alias mk='sudo make clean install'

# Git aliases
alias gu='git add -u'
alias ga='git add'
alias gaa='git add .'
alias gc='git commit -m'
alias gpull='git pull origin'
alias gp='git push origin'
alias gs='git status'

# Merge Xresources
alias merge='xrdb -merge ~/.Xresources'
[[ $- != *i* ]] && return

# Install fonts
alias fontinstall='sudo fc-cache -f -v'

# Some other alias
alias grep='grep --colour=auto'
alias egrep='egrep --colour=auto'
alias fgrep='fgrep --colour=auto'
alias cp="cp -i"                        # confirm before overwriting something
alias mv="mv -i" 
alias df='df -h'                        # human-readable sizes
alias free='free -m'                    # show sizes in MB
alias more=less

# Adjust clock
alias setclock='sudo timedatectl --adjust-system-clock set-local-rtc true'



# bind -m vi-insert "\C-l":clear-screen			# Enacle ctrl+l when vimode is set
