call plug#begin('~/vimfiles/plugged')

" Plug 'itchyny/lightline.vim'
Plug 'vim-airline/vim-airline'
Plug 'tpope/vim-fugitive'
Plug 'ap/vim-css-color'
Plug 'vifm/vifm.vim'
Plug 'tpope/vim-fugitive'
Plug 'gorodinskiy/vim-coloresque'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'vim-python/python-syntax'
Plug 'morhetz/gruvbox'
Plug 'tpope/vim-commentary'
Plug 'jiangmiao/auto-pairs'
Plug 'vimwiki/vimwiki'
" Plug 'SirVer/ultisnips'

call plug#end()

" Set font for Gvim
if has("gui_running")
	if has("gui_gtk2")
		set guifont=Inconsolata\ 12
	elseif has("gui_macvim")
		set guifont=Menlo\ Regular:h14
	elseif has("gui_win32")
		set guifont=Hack:h14:cANSI:qDRAFT
	endif
endif

"""""""""""""""
" => Remap Keys
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Remap F3 to set "noh"
nnoremap <ESC> :noh<CR>
inoremap jk <ESC>
" Map F5 to save the current file
map <F5> :w<CR>
" map <C-m> :call StripTrailingWhitespace()<CR>

function StripTrailingWhitespace()
	if !&binary && &filetype != 'diff'
		normal mz
		normal Hmy
		%s/\s\+$//e
		normal 'yz<CR>
		normal `z
	endif
endfunction

nnoremap <C-m> :call StripTrailingWhitespace()<CR>

" Map F10 to save and quit from the file
map <F10> :wq<CR>

set laststatus=2
set noshowmode

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Gruvbox settings "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:gruvbox_contrast_dark = 'medium'
colorscheme gruvbox

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

filetype plugin on
syntax enable
set nocompatible
set number relativenumber
set wildmenu		        	" Display all matches when tab complete.
set incsearch                   " Incremental search
set nobackup                    " No auto backups
set noswapfile                  " No swap
set expandtab                   " Use spaces instead of tabs.
set autoindent
set smartindent
set tabstop=4
set shiftwidth=4                " One tab == four spaces.
set tabstop=4                   " One tab == four spaces.
set softtabstop=4
set path+=**                    " Searches current directory recursively.
set paste
set t_Co=256                    " Set if term supports 256 colors.
set termguicolors
set mousemodel=extend
set scrolloff=10
set mouse=a
set splitbelow splitright
set hidden                      " Allow change to another buffer without have to save the current one
set backspace=indent,eol,start
set guioptions -=m
set guioptions -=T
set guioptions -=l
set guioptions -=r

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Python
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:python_highlight_all = 1

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Leader key
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let mapleader=" "
nnoremap <SPACE> <Nop>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Splits
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nnoremap <Leader>vn :vsplit
" Remap splits navigation with Leader + hjkl
nnoremap <Leader>h <C-w>h
nnoremap <Leader>j <C-w>j
nnoremap <Leader>k <C-w>k
nnoremap <Leader>l <C-w>l

" Set Space + wc to close focus split
nnoremap <Leader>wc <C-w>c
nnoremap <Leader>wo <C-w>o
imap <C-q> :q<CR>

" Make adjusing split sizes a bit more friendly
noremap <silent> <C-Left> :vertical resize +3<CR>
noremap <silent> <C-Right> :vertical resize -3<CR>
noremap <silent> <C-Up> :resize +3<CR>
noremap <silent> <C-Down> :resize -3<CR>

" Open a terminal in horizontal split
nnoremap <Leader>ty :terminal<CR>

" Change 2 split windows from vert to horiz or horiz to vert
nnoremap <Leader>th <C-w>t<C-w>H
nnoremap <Leader>tk <C-w>t<C-w>K

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Explore
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
noremap <Leader>ex :!explorer .<CR>
noremap <Leader>vs :Vexplore<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map <C-z> ysiw
" Delete with Control-h on insert mode
inoremap <C-h> <Left>
" Delete selected text and paste content on "" register
vnoremap <Leader>p "_dP
vnoremap <Leader>d "_d
nnoremap <Leader>d "_d
" Paste from clipboard
nnoremap <C-p> "*p
" Copy to clipboard
vnoremap <C-c> "*y
nnoremap <C-c> "*y

" Bind 'ESC' to go to normal mode on terminal
tnoremap <Esc> <C-\><C-n>
" Bind Control-h to go to normal mode and switch to left window
" tnoremap <Leader>h <C-\><C-N><C-w>h
" Bind Control-h to go to normal mode and switch to up window
" tnoremap <Leader>k <C-\><C-N><C-w>k

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Macros
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
autocmd! bufwritepost .vimrc source % | echom "Reloaded .vimrc"
autocmd bufwritepost % call StripTrailingWhitespace()
autocmd FileType vimwiki inoremap ;b ****<Esc>hi
autocmd FileType python nnoremap <Leader>cc :!py %<CR>
autocmd FileType go nnoremap <Leader>cc :!go run %<CR>
autocmd FileType javascript nnoremap <Leader>cc :!node %<CR>
autocmd FileType autohotkey setlocal commentstring=;\ %s

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Buffers
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Switch to next buffer
nnoremap <Leader>bn :bn<CR>
" Switch to previous buffer
nnoremap <Leader>bp :bp<CR>
nnoremap <Leader>bk :bw!<CR>
nnoremap <Leader>bl :buffers<CR> :buffer<Space>
nnoremap <Leader>cd :lcd %:p:h<CR>

map <C-j> <C-w>j
tnoremap <C-k> <C-w>k

" Edit shortcut
nnoremap <Leader>en :e C:\Users\lucas\AppData\Local\nvim\init.vim<CR>
nnoremap <Leader>ep :e C:\Users\lucas\Projects\
nnoremap <Leader>ev :e C:\Users\lucas\.vimrc<CR>

